package spiel2048.controller;

public interface IView {
    public void drawTitleScreen();
    public void setupGame();
    public void drawGame(int[] grid);
    public void drawGameOver(int score);
}
