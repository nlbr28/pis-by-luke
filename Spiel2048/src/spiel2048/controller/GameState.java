package spiel2048.controller;

public enum GameState {
    TITLE_SCREEN,
    GAME,
    GAME_OVER
}
