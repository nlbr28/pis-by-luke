package spiel2048.controller;

import spiel2048.model.Game2048;
import java.util.Arrays;

public class Controller implements IController {

    private Game2048 model;
    private IView view;
    private GameState state;

    public Controller(IView view) {
        this.view = view;
        this.model = new Game2048();
        this.state = GameState.TITLE_SCREEN;
    }
    public void nextFrame() {
        switch(state) {
            case TITLE_SCREEN -> {
                view.drawTitleScreen();
            }
            case GAME -> {
                view.drawGame(model.grid);
                if(model.is_game_over(model.grid)) state = GameState.GAME_OVER;
            }
            case GAME_OVER -> {
                view.drawGameOver(model.score);
            }
        }
    }
    public void userInput(Direction direction) {
        switch(state) {
            case TITLE_SCREEN -> {
                view.setupGame();
                state = GameState.GAME;
                return; //userInput bricht ab und es wird noch keine neue Tile erzeugt
            }
            case GAME -> {
                int[] temp_grid = new int[model.grid.length];
                //arrayCopy(model.grid, temp_grid);
                System.arraycopy(model.grid, 0, temp_grid, 0, model.grid.length);
                if(model.game) {
                    switch(direction) {
                        case LEFT:
                            model.score += model.move(model.grid);
                            break;
                        case RIGHT:
                            model.rotate(model.grid,2);
                            model.score += model.move(model.grid);
                            model.rotate(model.grid,2);
                            break;
                        case UP:
                            model.rotate(model.grid);
                            model.score += model.move(model.grid);
                            model.rotate(model.grid,3);
                            break;
                        case DOWN:
                            model.rotate(model.grid,3);
                            model.score += model.move(model.grid);
                            model.rotate(model.grid);
                    }
                }
                if(!Arrays.equals(model.grid,temp_grid)) {
                    model.random_tile(model.grid);
                    System.out.println("SCORE =" + model.score);
                }
                if(model.is_game_over(model.grid)) {
                    model.game = false;
                    System.out.println("GAME OVER. YOUR SCORE =" + model.score);
                }
            }
        }
    }
    public void setupGrid() {
        model.random_tile(model.grid);
        model.random_tile(model.grid);
        view.drawGame(model.grid);
    }
}