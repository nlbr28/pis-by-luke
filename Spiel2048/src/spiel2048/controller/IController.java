package spiel2048.controller;

public interface IController  {
    public void nextFrame();
    public void userInput(Direction direction);
    public void setupGrid();
}
