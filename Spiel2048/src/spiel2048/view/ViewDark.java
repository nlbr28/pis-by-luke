package spiel2048.view;

import spiel2048.controller.Controller;
import spiel2048.controller.IController;
import spiel2048.controller.IView;
import processing.core.PApplet;
import spiel2048.controller.Direction;

public class ViewDark extends PApplet implements IView {
    public static void main(String[] args) {
        PApplet.main(ViewDark.class);
    }
    private IController controller;
    private final int X_POS = 0;
    private final int Y_POS = 0;
    private final int X_OFFSET = 20;
    private final int Y_OFFSET = 20;

    private final int SIZE_TILE = 80;
    private final int SIZE_BORDER = 10;

    private final int X_SIZE = 2*X_POS+2*X_OFFSET+SIZE_BORDER+4*(SIZE_TILE+SIZE_BORDER);
    private final int Y_SIZE = 2*Y_POS+2*Y_OFFSET+SIZE_BORDER+4*(SIZE_TILE+SIZE_BORDER);

    public void settings() {
        super.size(X_SIZE, Y_SIZE);
    }
    public void setup() {
        controller = new Controller(this);
    }
    public void draw() {
        controller.nextFrame();
    }
    public void keyPressed() {
        if (key == CODED) {
            switch(keyCode) {
                case LEFT:
                    controller.userInput(Direction.LEFT);
                    break;
                case RIGHT:
                    controller.userInput(Direction.RIGHT);
                    break;
                case UP:
                    controller.userInput(Direction.UP);
                    break;
                case DOWN:
                    controller.userInput(Direction.DOWN);
            }
        }
    }
    public void drawTitleScreen() {
        super.background(color(0, 0, 0));
        super.textAlign(CENTER, CENTER);
        super.noStroke();
        super.textSize(64);
        super.text("Game Start", X_SIZE/2, Y_SIZE/2);
        super.textSize(16);
        super.text("Press any key to start", X_SIZE/2, Y_SIZE/2+45);
        super.fill(255);
    }
    public void setupGame() {
        super.textAlign(CENTER, CENTER);
        super.textSize(27);
        super.noStroke();
        super.background(color(0, 0, 0));
        super.colorMode(HSB, 360, 100, 100);

        controller.setupGrid();
    }
    public void drawGame(int[] grid) {
        //int edge_length = int(sqrt(grid.length));
        int edge_length = (int)sqrt(grid.length);
        int i = 0;
        int X, Y;
        for (int y=0; y<edge_length; y++) {
            Y = Y_POS+Y_OFFSET+SIZE_BORDER+y*(SIZE_TILE+SIZE_BORDER);
            for (int x=0; x<edge_length; x++) {
                X = X_POS+X_OFFSET+SIZE_BORDER+x*(SIZE_TILE+SIZE_BORDER);
                //fill(color(179, 189, 214));
                super.fill(color(30+log(grid[i]+1)/log(2)*10, 100, 100));
                super.rect(X, Y, SIZE_TILE, SIZE_TILE, 15);
                if (grid[i] != 0) {
                    super.fill(color(271, 0, 1));
                    super.text(grid[i], X+SIZE_TILE/2+1, Y+SIZE_TILE/2+1);
                }
                i++;
            }
        }
    }
    public void drawGameOver(int score) {
        super.colorMode(RGB, 255, 255, 255);
        super.background(color(0, 0, 0));
        super.textAlign(CENTER, CENTER);
        super.noStroke();
        super.textSize(64);
        super.text("Gameover", X_SIZE/2, Y_SIZE/2);
        super.textSize(16);
        super.text("Score: " + score, X_SIZE/2, Y_SIZE/2+45);
        super.fill(255);
    }
}
