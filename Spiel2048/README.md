Spiel2048 by @nlbr28, 11-05-2022 \
http://opensource.org/licenses/BSD-3-Clause \
\
This is an implementation of 2048 with Ligth- and Dark-mode \
\
 Use arrow keys (left, right, up and down) to move tiles on the grid. \
 Game ends if the tiles cannot be moved anymore.