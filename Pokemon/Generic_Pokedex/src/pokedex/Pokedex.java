package pokedex;

import pokemon.Pokemon;

import java.util.*;

public class Pokedex<T extends Nameable<T>> {

    String owner;
    public Map<String, T> pokemonMap = new HashMap<String, T>();
    public Pokedex(String owner) {
        this.owner = owner;
    }
    public void add(T nameable) {
        if(pokemonMap.get(nameable.returnName(nameable)) != null) return;
        pokemonMap.put(nameable.returnName(nameable), nameable);
    }

    public void swap(String name, Pokedex<T> other, String otherName) {
        if(pokemonMap.get(name) == null || other.pokemonMap.get(otherName) == null) return;
        var temp = pokemonMap.get(name);
        pokemonMap.remove(name);
        pokemonMap.put(otherName, other.pokemonMap.get(otherName));
        other.pokemonMap.remove(otherName);
        other.pokemonMap.put(name, temp);
        System.out.println(name + " --> " + other.owner);
        System.out.println(otherName + " --> " + owner + "\n");
    }

    public Set<T> getUniqueObjectsOf(Pokedex<T> other) {
        Set<T> returnSet = new HashSet<T>();
        Set<String> pokemon = other.pokemonMap.keySet();
        for(String p : pokemon) {
            if(pokemonMap.get(p) == null) returnSet.add(other.pokemonMap.get(p));
        }
        return returnSet;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(this.owner + " Pokedex:");
        result.append(System.lineSeparator());
        for (var p : pokemonMap.values()) {
            result.append("- ");
            result.append(p.toString());
            result.append(System.lineSeparator());
        }
        return result.toString();
    }
}
