package pokedex;

public interface Nameable<T> {
    default String returnName(T pokemon) {
        return pokemon.getClass().getSimpleName();
    }
}
