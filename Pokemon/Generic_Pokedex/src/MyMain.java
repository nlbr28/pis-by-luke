import pokedex.Pokedex;
import pokemon.*;

public class MyMain {
    public static void main(String[] args) {

        Pokedex<Pokemon> pokedexLuke = new Pokedex<Pokemon>("Luke");
        pokedexLuke.add(new Habitak(50,3));
        pokedexLuke.add(new Glurak(100,5));

        Pokedex<Pokemon> pokedexNils = new Pokedex<Pokemon>("Nils");
        pokedexNils.add(new Lavados(200,12));
        pokedexNils.add(new Glurak(100,5));

        System.out.println(pokedexLuke);
        System.out.println(pokedexNils);

        System.out.println("--------------------- SWAP ---------------------");
        pokedexLuke.swap("Habitak", pokedexNils, "Lavados");
        System.out.println(pokedexLuke);
        System.out.println(pokedexNils);
        System.out.println("------------------------------------------------\n");

        System.out.println(pokedexLuke.getUniqueObjectsOf(pokedexNils));
    }
}
