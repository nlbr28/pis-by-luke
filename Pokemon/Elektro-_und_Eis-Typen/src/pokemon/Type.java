package pokemon;

/*
Customization of the types orientated from the table of the PokeWiki
https://www.pokewiki.de/Typen
 */
interface PokemonType {
    boolean isWeakAgainst(Pokemon other);

    boolean isStrongAgainst(Pokemon other);
}

interface FlyingType extends PokemonType {
    default boolean isWeakAgainst(Pokemon other) {
        return other instanceof ElectricType;
    }

    default boolean isStrongAgainst(Pokemon other) {
        return other instanceof GrassType;
    }
}

interface FireType extends PokemonType {
    default boolean isWeakAgainst(Pokemon other) {
        return other instanceof FireType || other instanceof WaterType;
    }

    default boolean isStrongAgainst(Pokemon other) {
        return other instanceof GrassType || other instanceof IceType;
    }
}

interface WaterType extends PokemonType {
    default boolean isWeakAgainst(Pokemon other) {
        return other instanceof WaterType || other instanceof GrassType;
    }

    default boolean isStrongAgainst(Pokemon other) {
        return other instanceof FireType;
    }
}

interface GrassType extends PokemonType {
    default boolean isWeakAgainst(Pokemon other) {
        return other instanceof FlyingType || other instanceof FireType || other instanceof GrassType;
    }

    default boolean isStrongAgainst(Pokemon other) {
        return other instanceof WaterType;
    }
}

/* Implementation of ElectricType and IceType */
interface ElectricType extends PokemonType {
    default boolean isWeakAgainst(Pokemon other) { return other instanceof GrassType || other instanceof ElectricType; }

    default boolean isStrongAgainst(Pokemon other) { return other instanceof FlyingType || other instanceof WaterType; }
}

interface IceType extends PokemonType {
    default boolean isWeakAgainst(Pokemon other) { return other instanceof FireType || other instanceof WaterType || other instanceof IceType; }

    default boolean isStrongAgainst(Pokemon other) { return other instanceof FlyingType || other instanceof GrassType; }
}
