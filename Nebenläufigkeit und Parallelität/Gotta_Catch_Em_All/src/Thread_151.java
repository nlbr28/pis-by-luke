import processing.core.PApplet;
import processing.core.PImage;

public class Thread_151 extends PApplet {
    public static void main(String[] args) {
        PApplet.main(Thread_151.class);
    }

    PImage[] gen1;
    int size = 100;
    final int NUMBER_OF_THREADS = 151;

    @Override
    public void settings() {
        size(1500, 1100);
    }

    @Override
    public void setup() {
        gen1 = new PImage[151];
        LoadPImageThread[] threads = new LoadPImageThread[NUMBER_OF_THREADS];
        for (int i = 0; i < NUMBER_OF_THREADS; i++) {
            threads[i] = new LoadPImageThread(i, i);
            threads[i].start();
        }
    }
    @Override
    public void draw() {
        background(255);
        for(int i=0; i<gen1.length; i++) {
            int xPos = (int)(i % 15) * size;
            int yPos = (int)(i / 15) * size;
            fill(0);
            text("X", xPos+size/2, yPos+size/2);
            if(gen1[i]!=null)
                image(gen1[i], xPos, yPos, size, size);
        }
    }

     public class LoadPImageThread extends Thread {
        int startIndex;
        int endIndex;

        LoadPImageThread(int startIndex, int endIndex) {
            this.startIndex = startIndex;
            this.endIndex = endIndex;
        }

        @Override
        public void run() {
            while(startIndex <= endIndex) {
                String p = String.format("https://assets.pokemon.com/assets/cms2/img/pokedex/full/%03d.png", startIndex+1);
                gen1[startIndex] = loadImage(p);
                System.out.println("Loaded: "+ startIndex);
                startIndex++;
            }
        }
    }
}