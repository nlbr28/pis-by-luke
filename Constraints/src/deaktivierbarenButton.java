import controlP5.*;
import processing.core.PApplet;

import static controlP5.ControlP5Constants.ACTION_RELEASE;

public class deaktivierbarenButton extends PApplet {
    private Button button;
    private boolean state = true;

    public deaktivierbarenButton(Button button, CallbackListener callbackListener, int[] pos) {
        this.button = button;
        button.addListenerFor(ACTION_RELEASE, callbackListener);
        MySetup(pos);
    }

    private void MySetup(int[] pos) {
        button.setPosition(pos[0], pos[1]);
        button.setSize(300, 75);
        button.getCaptionLabel().setSize(15);
        button.setColorBackground(super.color(150));
    }

    public boolean isEnabled() {
        return state;
    }

    public void setDisabled(boolean state) {
        if(state) {
            this.state = false;
            button.setColorBackground(super.color(150, 150, 150, 100));
            button.getCaptionLabel().setColor(super.color(0, 50));
            button.setLabel("Disabled Button");
            button.lock();
        }else {
            this.state = true;
            System.out.println(button);
            button.setColorBackground(super.color(150, 150, 150));
            button.getCaptionLabel().setColor(super.color(250));
            button.setLabel("Enabled Button");
            button.unlock();
        }
    }

}
