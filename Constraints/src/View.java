import controlP5.*;
import processing.core.PApplet;

public class View extends PApplet {
    private ControlP5 cp5;
    private CallbackListener myCallBackListener;
    private ControlListener mycontrolListener;
    private deaktivierbarenButton buttim;
    private deaktivierbarenSlider brisliderus;

    private Button button;

    public static void main(String[] args) {
        PApplet.main(View.class);
    }

    public void settings() {
        super.setSize(400, 300);
    }

    public void setup() {
        cp5 = new ControlP5(this);

        myCallBackListener = (CallbackListener) -> {};
        mycontrolListener = (ControlListener) -> {};

        buttim = new deaktivierbarenButton(cp5.addButton("Enabled Button"), myCallBackListener, new int[] {50, 50});
        brisliderus = new deaktivierbarenSlider(cp5.addSlider("Enabled Slider"), mycontrolListener, new int[] {50, 200});

    }

    public void keyPressed() {
        buttim.setDisabled(buttim.isEnabled());
        brisliderus.setDisabled(brisliderus.isEnabled());
    }

    public void draw() {
        super.background(0);
    }

}
