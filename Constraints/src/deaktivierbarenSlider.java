import controlP5.*;
import processing.core.PApplet;

import static controlP5.ControlP5Constants.ACTION_RELEASE;

public class deaktivierbarenSlider extends PApplet {
    private Slider slider;
    private boolean state = true;

    public deaktivierbarenSlider(Slider slider, ControlListener controlListener, int[] pos) {
        this.slider = slider;
        slider.addListener(controlListener);
        MySetup(pos);
    }

    private void MySetup(int[] pos) {
        slider.setPosition(pos[0], pos[1]);
        slider.setSize(300, 75);
        slider.setLabel("");
        slider.setColorActive(super.color(89, 101, 217, 150));
        slider.setColorForeground(super.color(89, 101, 217, 150));
        slider.setColorBackground(super.color(150));
    }

    public boolean isEnabled() {
        return state;
    }

    public void setDisabled(boolean state) {
        if(state) {
            this.state = false;
            slider.setColorBackground(super.color(150, 150, 150, 100));
            slider.setColorForeground(super.color(89, 101, 217, 50));
            slider.getCaptionLabel().setColor(super.color(0, 50));
            slider.lock();
        }else {
            this.state = true;
            System.out.println(slider);
            slider.setColorBackground(super.color(150, 150, 150));
            slider.setColorForeground(super.color(89, 101, 217, 150));
            slider.getCaptionLabel().setColor(super.color(250));
            slider.unlock();
        }
    }

}
